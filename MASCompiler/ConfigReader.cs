﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASCompiler
{
    class ConfigReader
    {
        private string configFile;
        private List<string> file = new List<string>();

        private Dictionary<string, string> rfcmpSettings;

        public ConfigReader(string configFile)
        {
            this.configFile = configFile;
            if (!File.Exists(configFile))
                throw new ArgumentException("Only existing files can be used!");

            StreamReader reader = new StreamReader(configFile);
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Replace("\t", " ").Trim();
                if (!line.StartsWith("//") && line != "")
                    file.Add(line);
            }
            reader.Close();

            rfcmpSettings = new Dictionary<string, string>();
            string[] keys = { "name", "type", "version", "author", "url", "desc", "category", "origin", "baseversion" };

            foreach (string k in keys)
                rfcmpSettings.Add(k, "");
        }

        public RfBuild[] read()
        {
            List<RfBuild> list = new List<RfBuild>();
            int start = 0;
            while (start != -1)
            {
                list.Add(readSingle(start));
                start = getStart(start + 1);
            }

            return list.ToArray();
        }

        private RfBuild readSingle(int offset)
        {
            RfBuild build = new RfBuild();
            build.MASConfigs = new List<MASConfig>();

            int start = getStart(offset);
            if (start == -1)
                throw new FormatException("No start");

            int end = getStart(start + 1);
            if (end == -1)
                end = file.Count; //the last pack in the file

            build.buildPath = Util.getValuePair(file[start]).Value;

            build.buildPath = build.buildPath.Trim();

            //Reading the rfcmp info
            build.cmpinfo = getPackingInfo(start, end);

            for (int i = start; i < end; i++)
            {
                i = getSectionStart(i, end);
                if (i == -1)
                    break;

                string masname = file[i - 1];
                if (masname != null && masname.ToLower().Contains(".mas"))
                {
                    MASConfig config = new MASConfig();
                    config.MasLocation = masname;
                    int sectionStart = i+1;
                    i = getSectionEnd(i, end);
                    config.rules = new string[i - sectionStart];
                    for (int j = sectionStart; j < i; j++)
                    {
                        config.rules[j- sectionStart] = file[j];
                    }

                    build.MASConfigs.Add(config);
                }
            }

            return build;
        }

        private Dictionary<string, string> getPackingInfo(int start, int end)
        {
            int i = getSectionStart(start, end) - 1;
            while (Util.getValuePair(file[i]).Key != "rfcmp")
            {
                i = getSectionStart(i + 2, end) - 1;
                if (i >= end || i == -1)
                    return null;
            }

            int sectionStart = getSectionStart(i, end) + 1;
            int sectionEnd = getSectionEnd(i, end);

            Dictionary<string, string> dict = Util.ListToDict(file, sectionStart, sectionEnd);

            if (rfcmpSettings != null)
                dict = Util.addDefaultValues(dict, rfcmpSettings);

            rfcmpSettings = dict;

            return dict;
        }

        //This is useful to distinguish between multiple rfcmp
        private int getStart(int offset)
        {
            for (int i = offset; i < file.Count; i++)
            {
                string key = Util.getValuePair(file[i]).Key;
                if (key == "buildpath")
                    return i;
            }
            return -1;
        }

        private int getSectionStart(int offset, int limit)
        {
            for (int i = offset; i < limit; i++)
            {
                string key = Util.getValuePair(file[i]).Key;
                if (key == "{")
                    return i;
            }

            return -1;
        }

        private int getSectionEnd(int offset, int limit)
        {
            for (int i = offset; i < limit; i++)
            {
                string key = Util.getValuePair(file[i]).Key;
                if (key == "}")
                    return i;
            }

            return -1;
        }

    }
}
