﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace TheLichten.General
{
    public partial class About : Form
    {
        public About(Form owner, string urlForVersions)
        {
            InitializeComponent();

            //Reads the newest version
            bool newer = false;

            try
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(urlForVersions);
                StreamReader reader = new StreamReader(stream);
                string line = reader.ReadLine();
                reader.Close();
                stream.Close();
                string version = line.Split(':')[1].Trim();
                int[] newVersion = getVersionNumber(version);
                int[] currentVersion = getVersionNumber(label2.Text.Replace("Version", "").Trim());


                for (int i = 0; i < newVersion.Length; i++)
                {
                    if (newVersion[i] > currentVersion[i])
                    {
                        newer = true;
                        break;
                    }
                    else if (currentVersion[i] > newVersion[i])
                    {
                        break;
                    }
                }
                this.lblNewVersionNumber.Text = version;
            }
            catch
            {

            }
            
            this.lblNewVersionLabel.Visible = newer;
            this.lblNewVersionNumber.Visible = newer;


            this.Visible = true;
            this.Owner = owner;
        }

        private int[] getVersionNumber(string versionString)
        {
            versionString = versionString.Trim();
            int[] version = new int[4];

            //beta version. Non beta versions have the highest value
            version[3] = Int32.MaxValue; 
            if (versionString.Contains('-'))
            {
                string value = versionString.Split('-')[1];
                value = value.ToLower().Replace("beta","").Trim();
                if (value == "")
                {
                    version[3] = 0;
                }
                else
                {
                    version[3] = Int32.Parse(value);
                }
                versionString = versionString.Split('-')[0].Trim();
            }

            string[] toConvert = versionString.Split('.');
            for (int i=0; i < 3 && i < toConvert.Length; i++)
            {
                version[i] = Int32.Parse(toConvert[i].Trim());
            }

            return version;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://forum.studio-397.com/index.php?threads/mascompiler-1-0-the-mod-packaging-tool.59365/");
        }

        private void linkMail_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("Mailto:lichtenrider@hotmail.de");
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/LukasLichten/MASCompiler");
        }
    }
}
